import {
  dictionary,
  locale,
  _
} from 'svelte-i18n';

//Import language files
import * as english from "./i18n/en.json";
import * as arabic from "./i18n/ar.json";

function setupI18n({
  withLocale: _locale
} = {
  withLocale: 'en'
}) {
  dictionary.set({
    en: english,
    ar: arabic,
  });
  locale.set(_locale);
}
export {
  _,
  setupI18n
};
