import * as _variables from "./variables.json";

/**
 * @type {boolean}
 */
import { disabled_value } from './stores.js';


/**
 * Send the schema to the external API and handle response
 * @param {Object} schema
 * @returns {Promise<Object>}
 */
export async function send_toml_to_api(schema) {
  const response = await fetch(_variables.schema_api_url, {
    method: "POST",
    body: JSON.stringify(schema),
    headers: {
      "Content-Type": "application/json",
    },
  });

  // check if response was ok
  if (response.ok) {
    console.log("Response is OK!");
    disabled_value.set(true);
    console.log("disabled", disabled_value)
    return await response.json();
  } else {
    // test = await _response.json();
    console.log("Response is NOT OK!")
    /** @type {any} */
    let error_message = "";
    //console.dir(response.json())
    const res = await response.json()
    if (Array.isArray(res.detail)) {
      console.log(res.detail);
      /** @type {Array<object>} */
      let pydantic_array = [];
      res.detail.forEach((item) => {
        pydantic_array.push(item.loc);
        console.log("item", item)
      });
      error_message = pydantic_array;
      console.log("response had detail item")
    } else if (res.detail) {
      console.log("response had detail")
      error_message = res.detail;
    } else {
      error_message = res.detail;
      console.log("latest else")
      error_message = res.detail;
    }
    throw new Error(error_message);
  }
}

/**
 * Handle submit of toml file url to the external API
 * @param {string} personal_toml_url
 */
export async function send_url_to_api(personal_toml_url) {
  const response = await fetch(_variables.url_submit_api, {
    method: "POST",
    body: JSON.stringify({ "url": personal_toml_url }),
    headers: {
      "Content-Type": "application/json",
    },
  });


  // check if response was ok
  if (response.ok) {
    disabled_value.set(true);
    console.log("disabled", disabled_value);
    return await response.json();
  } else {
    // console.log(clone)
    throw new Error('Request failed');
    // merge_request_text = "Missing fields!";
  }
}
